package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
